#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  1 12:08:43 2021

@author: vignesh.jayanth
"""


# Importing the libraries #

import pandas as pd
import numpy as np

# Splitting the 50_50 column #

def data_split(df):

    #df['50_50'] = df['50_50'].fillna({i: {} for i in df.index})  # if the column is not strings
    #df = df.join(pd.json_normalize(df['50_50']))

    # drop 50_50
    df.drop(columns=['50_50'], inplace=True)

    # Splitting the tactics column #
    df['tactics'] = df['tactics'].fillna({i: {} for i in df.index})  # if the column is not strings
    df = df.join(pd.json_normalize(df['tactics']))

    # drop tactics
    df.drop(columns=['tactics'], inplace=True)

    # Splitting the location column into different Start x, Start y and Start Z columns
    df['location'] = df['location'].fillna({i: [] for i in df.index})  # if the column is not strings
    df[['Start_x','Start_y','Start_z']] = pd.DataFrame(df['location'].tolist(), index= df.index).fillna(0)

    # Splitting the location column into different End x, End y and End Z columns
    # Fill rows as nan if they aren't of type dict to keep it coherent

    df['pass_end_location'] = df['pass_end_location'].fillna({i: [] for i in df.index})  # if the column is not strings
    df['goalkeeper_end_location'] = df['goalkeeper_end_location'].fillna({i: [] for i in df.index})  # if the column is not strings
    df['carry_end_location'] = df['carry_end_location'].fillna({i: [] for i in df.index})  # if the column is not strings
    df['shot_end_location'] = df['shot_end_location'].fillna({i: [] for i in df.index})  # if the column is not strings

    # Split the location rows in a list to dataframe #
    df[['goalkeeper_end_x','goalkeeper_end_y']] = pd.DataFrame(df['goalkeeper_end_location'].tolist(), index= df.index).fillna('')
    df[['pass_end_x','pass_end_y']] = pd.DataFrame(df['pass_end_location'].tolist(), index= df.index).fillna('')
    df[['carry_end_x','carry_end_y']] = pd.DataFrame(df['carry_end_location'].tolist(), index= df.index).fillna('')
    df[['shot_end_x','shot_end_y','End_z']] = pd.DataFrame(df['shot_end_location'].tolist(), index= df.index).fillna('')

    # Combine the necessary columns into one for uniformity #
    df['End_x'] = df[['goalkeeper_end_x','pass_end_x',
                               'carry_end_x','shot_end_x']].apply(lambda row: ''.join(row.values.astype(str)), axis=1)
    
    df['body_part'] = df[['clearance_body_part', 'goalkeeper_body_part', 'pass_body_part','shot_body_part']].apply(lambda row: ''.join(row.values.astype(str)), axis=1)

    df['body_part'] = df['body_part'].str.replace('None','')
    
    df['pass_type'] = df[['pass_cross', 'pass_cut_back', 'pass_switch','pass_goal_assist','pass_shot_assist']].apply(lambda row: ''.join(row.values.astype(str)), axis=1)

    df['body_part'] = df['body_part'].str.replace('None','')
    
    df['pass_type'] = df['pass_type'].str.replace('None','')
    
    df['End_y'] = df[['goalkeeper_end_y','pass_end_y',
                               'carry_end_y','shot_end_y']].apply(lambda row: ''.join(row.values.astype(str)), axis=1)
    
    # Combining Minute and Second to form Time
    
    df['Time']=(df['minute'])+(df['second']/60)


    # Drop unnecessary columns #
    df=df.drop(['goalkeeper_end_x','pass_end_x','carry_end_x','shot_end_x','goalkeeper_end_y','pass_end_y',
              'carry_end_y','shot_end_y','location','carry_end_location','goalkeeper_end_location',
              'shot_end_location','pass_end_location'], axis=1)

    # Replace Blanks with np.nan's #
    df['End_x']=df['End_x'].replace('', np.nan).fillna(0)
    df['End_y']=df['End_y'].replace('', np.nan).fillna(0)
    df['End_x']=df['End_x'].astype(float)
    df['End_y']=df['End_y'].astype(float)
    df['End_y']=np.where(df['End_y']==0,df['Start_y'],df['End_y'])
    df['End_x']=np.where(df['End_x']==0,df['Start_x'],df['End_x'])
    
    return df


def convert_dim(df):
    df['Start_x']=df['Start_x']*0.868
    df['End_x']=df['End_x']*0.868
    df['Start_y']=df['Start_y']*0.84
    df['End_y']=df['End_y']*0.84
    
    return df


def assign_zones(df):
    
    #Assigning Proxy associated to x-axis (three Vertical zones)
    bins = [0,35,71,106]
    labels = ["Tiers-défensif","Tiers-Médian","Tiers-offensif"]
    df['x_zone']= pd.cut(df['Start_x'], bins=bins, labels=labels)

    #Identifying the next consecutive horizontal zones of play
    df['next_x_zone1']=df['x_zone'].shift(-1)
    df['next_x_zone2']=df['x_zone'].shift(-2)
    df['next_x_zone3']=df['x_zone'].shift(-3)
    df['next_x_zone4']=df['x_zone'].shift(-4)
    df['next_x_zone5']=df['x_zone'].shift(-5)


    #Assigning Proxy associated to the defensive and offensive box
    col_1         = 'Start_x'
    col_2         = 'Start_y'

    conditions  = [(df[col_1] <= 17.85) & (df[col_2] >= 14.34) & (df[col_2] <= 53.65),(df[col_1] >= 87.15) & (df[col_2] >= 14.34) & (df[col_2] <= 53.65)]
    choices     = [ "Defensive-surface", 'Offensif-surface']

    df["Box"] = np.select(conditions, choices, default='Open-Play')
    df["next_Box1"] = df["Box"].shift(-1)
    df["next_Box2"] = df["Box"].shift(-2)


    #Assigning Proxy associated to y-axis (5 horizontal corridors)
    bins1 = [0,14,25,43,54,68]
    labels1 = ["Gauche","Demi-espace Gauche","Centre","Demi-espace Droit","Droit"]

    df['y_zone']= pd.cut(df['Start_y'], bins=bins1, labels=labels1)

    #Identifying the next consecutive vertical corridors of play
    df['next_y_zone1']=df['y_zone'].shift(-1)
    df['next_y_zone2']=df['y_zone'].shift(-2)
    df['next_y_zone3']=df['y_zone'].shift(-3)
    df['next_y_zone4']=df['y_zone'].shift(-4)
    df['next_y_zone5']=df['y_zone'].shift(-5)

    # Identifying the Next "5 actions" in a possession sequence
    df['next_event1']=df['type'].shift(-1)
    df['next_event2']=df['type'].shift(-2)
    df['next_event3']=df['type'].shift(-3)
    df['next_event4']=df['type'].shift(-4)
    df['next_event5']=df['type'].shift(-5)

    # Identifying the previous "5 actions" in a possession sequence
    df['prev_event1']=df['type'].shift(1)
    df['prev_event2']=df['type'].shift(2)
    df['prev_event3']=df['type'].shift(3)
    df['prev_event4']=df['type'].shift(4)
    df['prev_event5']=df['type'].shift(5)


    # Identifying the Next "5 x-locations"
    df['next_x1']=df['Start_x'].shift(-1)
    df['next_x2']=df['Start_x'].shift(-2)
    df['next_x3']=df['Start_x'].shift(-3)
    df['next_x4']=df['Start_x'].shift(-4)
    df['next_x5']=df['Start_x'].shift(-5)

    # Identifying the previous "5 x-locations"
    df['prev_x1']=df['Start_x'].shift(1)
    df['prev_x2']=df['Start_x'].shift(2)
    df['prev_x3']=df['Start_x'].shift(3)
    df['prev_x4']=df['Start_x'].shift(4)
    df['prev_x5']=df['Start_x'].shift(5)


    # Identifying the Next "5 y-locations"

    df['next_y1']=df['Start_y'].shift(-1)
    df['next_y2']=df['Start_y'].shift(-2)
    df['next_y3']=df['Start_y'].shift(-3)
    df['next_y4']=df['Start_y'].shift(-4)
    df['next_y5']=df['Start_y'].shift(-5)

    # Identifying the previous "5 x-locations"
    df['prev_y1']=df['Start_y'].shift(1)
    df['prev_y2']=df['Start_y'].shift(2)
    df['prev_y3']=df['Start_y'].shift(3)
    df['prev_y4']=df['Start_y'].shift(4)
    df['prev_y5']=df['Start_y'].shift(5)


    # Identifying the Next "5 players involved in actions"
    df['player_1']=df['player'].shift(-1)
    df['player_2']=df['player'].shift(-2)
    df['player_3']=df['player'].shift(-3)
    df['player_4']=df['player'].shift(-4)
    df['player_5']=df['player'].shift(-5)


    # Identifying the previous "5 players involved in actions"
    df['player-1']=df['player'].shift(1)
    df['player-2']=df['player'].shift(2)
    df['player-3']=df['player'].shift(3)
    df['player-4']=df['player'].shift(4)
    df['player-5']=df['player'].shift(5)


    # Renaming 50°50 Columns for relevant outcomes
    df=df.rename(columns={'outcome.id': '50_50_outcome_id', 'outcome.name': '50_50_outcome_name'})
    
    return df

def play_pattern(df):

    #Subsetting data for open play and closed play
    df['play_pattern_count'] = df.groupby((df['play_pattern'] != df['play_pattern'].shift(1)).cumsum()).cumcount()+1
    df=df[~(df['type']=='Goal Keeper')].reset_index(drop=True)

    #Subsetting times for Goal Kicks, Corner Kicks, Free Kicks to sync with tracking data: #Miscellaneous Information
    GoalKicks=df[(df['play_pattern']=='From Goal Kick')|(df['play_pattern']=='From Keeper')]
    Corner=df[df['play_pattern']=='From Corner']
    FreeKicks=df[df['play_pattern']=='From Free Kick']
    Counter_Attack=df[df['play_pattern']=='From Counter']
    Regular_Play=df[(df['play_pattern']=='Regular Play')|(df['play_pattern']=='Other')].reset_index(drop=True)
    
    return GoalKicks,Corner,FreeKicks,Counter_Attack,Regular_Play

# Add distance between consecutive actions

def add_distance_to_ball(df):

    df['distance'] = df[['Start_x', 'Start_y']].sub(np.array(df[['next_x1', 'next_y1']] )).pow(2).sum(1).pow(0.5)
    df.distance = df.distance.round(2)

    return(df)

