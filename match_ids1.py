#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 12 10:24:58 2021

@author: vignesh.jayanth
"""


# Splitting the 50_50 column #

def data_split(Rennes):

    Rennes['50_50'] = Rennes['50_50'].fillna({i: {} for i in Rennes.index})  # if the column is not strings
    Rennes = Rennes.join(pd.json_normalize(Rennes['50_50']))

    # drop 50_50
    Rennes.drop(columns=['50_50'], inplace=True)

    # Splitting the tactics column #
    Rennes['tactics'] = Rennes['tactics'].fillna({i: {} for i in Rennes.index})  # if the column is not strings
    Rennes = Rennes.join(pd.json_normalize(Rennes['tactics']))

    # drop tactics
    Rennes.drop(columns=['tactics'], inplace=True)

    # Splitting the location column into different Start x, Start y and Start Z columns
    Rennes['location'] = Rennes['location'].fillna({i: [] for i in Rennes.index})  # if the column is not strings
    Rennes[['Start_x','Start_y','Start_z']] = pd.DataFrame(Rennes['location'].tolist(), index= Rennes.index).fillna(0)

    # Splitting the location column into different End x, End y and End Z columns
    # Fill rows as nan if they aren't of type dict to keep it coherent

    Rennes['pass_end_location'] = Rennes['pass_end_location'].fillna({i: [] for i in Rennes.index})  # if the column is not strings
    Rennes['goalkeeper_end_location'] = Rennes['goalkeeper_end_location'].fillna({i: [] for i in Rennes.index})  # if the column is not strings
    Rennes['carry_end_location'] = Rennes['carry_end_location'].fillna({i: [] for i in Rennes.index})  # if the column is not strings
    Rennes['shot_end_location'] = Rennes['shot_end_location'].fillna({i: [] for i in Rennes.index})  # if the column is not strings

    # Split the location rows in a list to dataframe #
    Rennes[['goalkeeper_end_x','goalkeeper_end_y']] = pd.DataFrame(Rennes['goalkeeper_end_location'].tolist(), index= Rennes.index).fillna('')
    Rennes[['pass_end_x','pass_end_y']] = pd.DataFrame(Rennes['pass_end_location'].tolist(), index= Rennes.index).fillna('')
    Rennes[['carry_end_x','carry_end_y']] = pd.DataFrame(Rennes['carry_end_location'].tolist(), index= Rennes.index).fillna('')
    Rennes[['shot_end_x','shot_end_y','End_z']] = pd.DataFrame(Rennes['shot_end_location'].tolist(), index= Rennes.index).fillna('')

    # Combine the necessary columns into one for uniformity #
    Rennes['End_x'] = Rennes[['goalkeeper_end_x','pass_end_x',
                               'carry_end_x','shot_end_x']].apply(lambda row: ''.join(row.values.astype(str)), axis=1)
    Rennes['End_y'] = Rennes[['goalkeeper_end_y','pass_end_y',
                               'carry_end_y','shot_end_y']].apply(lambda row: ''.join(row.values.astype(str)), axis=1)

    # Drop unnecessary columns #
    Rennes=Rennes.drop(['goalkeeper_end_x','pass_end_x','carry_end_x','shot_end_x','goalkeeper_end_y','pass_end_y',
              'carry_end_y','shot_end_y','location','carry_end_location','goalkeeper_end_location',
              'shot_end_location','pass_end_location'], axis=1)

    # Replace Blanks with np.nan's #
    Rennes['End_x']=Rennes['End_x'].replace('', np.nan).fillna(0)
    Rennes['End_y']=Rennes['End_y'].replace('', np.nan).fillna(0)
    Rennes['End_x']=Rennes['End_x'].astype(float)
    Rennes['End_y']=Rennes['End_y'].astype(float)
    Rennes['End_y']=np.where(Rennes['End_y']==0,Rennes['Start_y'],Rennes['End_y'])
    Rennes['End_x']=np.where(Rennes['End_x']==0,Rennes['Start_x'],Rennes['End_x'])
    
    return Rennes


def convert_dim(df):
    df['Start_x']=df['Start_x']*0.868
    df['End_x']=df['End_x']*0.868
    df['Start_y']=df['Start_y']*0.84
    df['End_y']=df['End_y']*0.84
    
    return df


def assign_zones(df3):
    
    #Assigning Proxy associated to x-axis (three Vertical zones)
    bins = [0,35,71,106]
    labels = ["Tiers-défensif","Tiers-Médian","Tiers-offensif"]
    df3['x_zone']= pd.cut(df3['Start_x'], bins=bins, labels=labels)

    #Identifying the next consecutive horizontal zones of play
    df3['next_x_zone1']=df3['x_zone'].shift(-1)
    df3['next_x_zone2']=df3['x_zone'].shift(-2)
    df3['next_x_zone3']=df3['x_zone'].shift(-3)
    df3['next_x_zone4']=df3['x_zone'].shift(-4)
    df3['next_x_zone5']=df3['x_zone'].shift(-5)


    #Assigning Proxy associated to the defensive and offensive box
    col_1         = 'Start_x'
    col_2         = 'Start_y'

    conditions  = [(df3[col_1] <= 17.85) & (df3[col_2] >= 14.34) & (df3[col_2] <= 53.65),(df3[col_1] >= 87.15) & (df3[col_2] >= 14.34) & (df3[col_2] <= 53.65)]
    choices     = [ "Defensive-surface", 'Offensif-surface']

    df3["Box"] = np.select(conditions, choices, default='Open-Play')
    df3["next_Box1"] = df3["Box"].shift(-1)
    df3["next_Box2"] = df3["Box"].shift(-2)


    #Assigning Proxy associated to y-axis (5 horizontal corridors)
    bins1 = [0,14,25,43,54,68]
    labels1 = ["Droit","Demi-espace Droit","Centre","Demi-espace Gauche","Gauche"]
    df3['y_zone']= pd.cut(df3['Start_y'], bins=bins1, labels=labels1)

    #Identifying the next consecutive vertical corridors of play
    df3['next_y_zone1']=df3['y_zone'].shift(-1)
    df3['next_y_zone2']=df3['y_zone'].shift(-2)
    df3['next_y_zone3']=df3['y_zone'].shift(-3)
    df3['next_y_zone4']=df3['y_zone'].shift(-4)
    df3['next_y_zone5']=df3['y_zone'].shift(-5)

    # Identifying the Next "5 actions" in a possession sequence
    df3['next_event1']=df3['type'].shift(-1)
    df3['next_event2']=df3['type'].shift(-2)
    df3['next_event3']=df3['type'].shift(-3)
    df3['next_event4']=df3['type'].shift(-4)
    df3['next_event5']=df3['type'].shift(-5)

    # Identifying the previous "5 actions" in a possession sequence
    df3['prev_event1']=df3['type'].shift(1)
    df3['prev_event2']=df3['type'].shift(2)
    df3['prev_event3']=df3['type'].shift(3)
    df3['prev_event4']=df3['type'].shift(4)
    df3['prev_event5']=df3['type'].shift(5)


    # Identifying the Next "5 x-locations"
    df3['next_x1']=df3['Start_x'].shift(-1)
    df3['next_x2']=df3['Start_x'].shift(-2)
    df3['next_x3']=df3['Start_x'].shift(-3)
    df3['next_x4']=df3['Start_x'].shift(-4)
    df3['next_x5']=df3['Start_x'].shift(-5)

    # Identifying the previous "5 x-locations"
    df3['prev_x1']=df3['Start_x'].shift(1)
    df3['prev_x2']=df3['Start_x'].shift(2)
    df3['prev_x3']=df3['Start_x'].shift(3)
    df3['prev_x4']=df3['Start_x'].shift(4)
    df3['prev_x5']=df3['Start_x'].shift(5)


    # Identifying the Next "5 y-locations"

    df3['next_y1']=df3['Start_y'].shift(-1)
    df3['next_y2']=df3['Start_y'].shift(-2)
    df3['next_y3']=df3['Start_y'].shift(-3)
    df3['next_y4']=df3['Start_y'].shift(-4)
    df3['next_y5']=df3['Start_y'].shift(-5)

    # Identifying the previous "5 x-locations"
    df3['prev_y1']=df3['Start_y'].shift(1)
    df3['prev_y2']=df3['Start_y'].shift(2)
    df3['prev_y3']=df3['Start_y'].shift(3)
    df3['prev_y4']=df3['Start_y'].shift(4)
    df3['prev_y5']=df3['Start_y'].shift(5)


    # Identifying the Next "5 players involved in actions"
    df3['player_1']=df3['player'].shift(-1)
    df3['player_2']=df3['player'].shift(-2)
    df3['player_3']=df3['player'].shift(-3)
    df3['player_4']=df3['player'].shift(-4)
    df3['player_5']=df3['player'].shift(-5)


    # Identifying the previous "5 players involved in actions"
    df3['player-1']=df3['player'].shift(1)
    df3['player-2']=df3['player'].shift(2)
    df3['player-3']=df3['player'].shift(3)
    df3['player-4']=df3['player'].shift(4)
    df3['player-5']=df3['player'].shift(5)


    # Renaming 50°50 Columns for relevant outcomes
    df3=df3.rename(columns={'outcome.id': '50_50_outcome_id', 'outcome.name': '50_50_outcome_name'})
    
    return df3

def play_pattern(df3):

    #Subsetting data for open play and closed play
    df3['play_pattern_count'] = df3.groupby((df3['play_pattern'] != df3['play_pattern'].shift(1)).cumsum()).cumcount()+1
    df3=df3[~(df3['type']=='Goal Keeper')].reset_index(drop=True)

    #Subsetting times for Goal Kicks, Corner Kicks, Free Kicks to sync with tracking data: #Miscellaneous Information
    GoalKicks=df3[(df3['play_pattern']=='From Goal Kick')|(df3['play_pattern']=='From Keeper')]
    Corner=df3[df3['play_pattern']=='From Corner']
    FreeKicks=df3[df3['play_pattern']=='From Free Kick']
    Counter_Attack=df3[df3['play_pattern']=='From Counter']
    Regular_Play=df3[(df3['play_pattern']=='Regular Play')|(df3['play_pattern']=='Other')].reset_index(drop=True)
    
    return GoalKicks,Corner,FreeKicks,Counter_Attack,Regular_Play

# Add distance between consecutive actions

def add_distance_to_ball(df3):

    df3['distance'] = df3[['Start_x', 'Start_y']].sub(np.array(df3[['next_x1', 'next_y1']] )).pow(2).sum(1).pow(0.5)
    df3.distance = df3.distance.round(2)

    return(df3)

