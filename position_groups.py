#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  3 16:19:48 2021

@author: vignesh.jayanth
"""

import pandas as pd

def statsbomb_pos():
    

    Positions = {"Milieu Centrale": ['Right Center Midfield','Left Center Midfield',
                                        'Center Midfield'],
                 "Milieu Offensive":['Center Attacking Midfield','Right Attacking Midfield', 
                                         'Left Attacking Midfield'],
                "Défenseur Central":['Right Center Back','Left Center Back','Center Back'],
                 "Ailiers":['Left Wing', 'Right Wing','Left Midfield','Right Midfield'],
                 "Attaquant":['Left Center Forward','Center Forward','Right Center Forward'],
                 "Latéral":['Left Back','Right Back','Left Wing Back','Right Wing Back'],
                 "Milieu Défensif":['Center Defensive Midfield','Left Defensive Midfield','Right Defensive Midfield'],
                 'Gardien':['Goalkeeper']}
    
    Positions1 = {"Central Midfielder": ['Right Center Midfield','Left Center Midfield',
                                    'Center Midfield'],
             "Attacking Midfielder":['Center Attacking Midfield','Right Attacking Midfield', 
                                     'Left Attacking Midfield'],
            "Center Back":['Right Center Back','Left Center Back','Center Back'],
             "Winger":['Left Wing', 'Right Wing','Left Midfield','Right Midfield'],
             "Forward":['Left Center Forward','Center Forward','Right Center Forward'],
             "Full Back":['Left Back','Right Back','Left Wing Back','Right Wing Back'],
             "Defensive Midfielder":['Center Defensive Midfield','Left Defensive Midfield','Right Defensive Midfield'],
             'Goalkeeper':['Goalkeeper']}
    
    
    
    pos=pd.DataFrame(pd.concat({k: pd.Series(v) for k, v in Positions.items()})).reset_index().set_axis(['position_group', 'NA', 'position_sb'], axis=1, inplace=False)
    del pos['NA']
    
    return pos


def modes(df, key_cols, value_col, count_col):
    '''                                                                                                                                                                                                                                                                                                                                                              
    Pandas does not provide a `mode` aggregation function                                                                                                                                                                                                                                                                                                            
    for its `GroupBy` objects. This function is meant to fill                                                                                                                                                                                                                                                                                                        
    that gap, though the semantics are not exactly the same.                                                                                                                                                                                                                                                                                                         

    The input is a DataFrame with the columns `key_cols`                                                                                                                                                                                                                                                                                                             
    that you would like to group on, and the column                                                                                                                                                                                                                                                                                                                  
    `value_col` for which you would like to obtain the modes.                                                                                                                                                                                                                                                                                                        

    The output is a DataFrame with a record per group that has at least                                                                                                                                                                                                                                                                                              
    one mode (null values are not counted). The `key_cols` are included as                                                                                                                                                                                                                                                                                           
    columns, `value_col` contains lists indicating the modes for each group,                                                                                                                                                                                                                                                                                         
    and `count_col` indicates how many times each mode appeared in its group.                                                                                                                                                                                                                                                                                        
    '''
    return df.groupby(key_cols + [value_col]).size() \
             .to_frame(count_col).reset_index() \
             .groupby(key_cols + [count_col])[value_col].unique() \
             .to_frame().reset_index() \
             .sort_values(count_col, ascending=False) \
             .drop_duplicates(subset=key_cols)
             
# Leagues removed- 'Europe - UEFA Youth League'

AgeLeagues=['Portugal - Segunda Liga', 'Italy - Italy Serie B','Argentina - Liga Profesional Argentina','Sweden - Allsvenskan', 'Austria - Bundesliga', 
            'Poland - Ekstraklasa', 'Ukraine - Premier League','Sweden - Superettan', 'England - Championship', 'Slovakia - Super Liga', 'Czech Republic - Liga', 
            'Portugal - Liga NOS', 'Spain - La Liga', 'Turkey - Süper Lig', 'Denmark - Superliga', 'France - Ligue 1',
            'Europe - UEFA Europa Conference League', 'Colombia - Primera A', 'Europe - Champions League',
            'Belgium - Jupiler Pro League', 'France - Ligue 2', 'Norway - Eliteserien', 'Spain - La Liga 2', 'Croatia - 1. HNL',
            'Uruguay - Primera División', 'Netherlands - Eerste Divisie', 'Brazil - Série A', 'Chile - Primera División', 
            'Mexico - Liga MX', 'Turkey - 1. Lig', 'Russia - Premier League', 'Serbia - Super Liga',  
            'United States of America - Major League Soccer',  'Finland - Veikkausliiga', 'Italy - Serie A', 
            'Greece - Super League', 'Peru - Primera División', 'Germany - 2. Bundesliga', 'Romania - Liga I', 
            'Scotland - Premier League', 'England - Premier League', 'Germany - DFB Pokal', 'Switzerland - Challenge League',
            'Germany - 1. Bundesliga', 'Ireland - Premier Division', 'Switzerland - Super League', 
            'Paraguay - Division Profesional','Netherlands - Eredivisie']