#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 12 10:24:58 2021

@author: vignesh.jayanth
"""


# Importing the libraries #

import matplotlib.patheffects as path_effects

import matplotlib.pyplot as plt
import pandas as pd
from pitch_official import draw_pitch
from matplotlib.colors import LinearSegmentedColormap
from mplsoccer import VerticalPitch, Pitch
from mplsoccer.cm import create_transparent_cmap
from mplsoccer.scatterutils import arrowhead_marker

from mplsoccer import VerticalPitch, add_image, FontManager
from mplsoccer.statsbomb import read_event, EVENT_SLUG

csfont = {'fontfamily':'Montserrat'}


# Visualize passess #
  
def plot_passes(TB):
    
    background = "#030E21"

    # For Horizontal View #
    fig, ax = plt.subplots(figsize=(11, 7))

    fig.set_facecolor(background)

    draw_pitch(orientation="h",
               aspect="full",
               pitch_color=background, 
               line_color="w",
               ax=ax)

    x = TB.Start_x.values
    y = TB.Start_y.values
    xe = TB.End_x.values
    ye = TB.End_y.values

    plt.scatter(xe,ye,color="w",edgecolors="w",zorder=20,alpha=1)
    plt.plot([x,xe],[y,ye],alpha=0.4,color="yellow")



    #plt.title("Lovro Majer: Through Passes Start Locations",fontsize=18,color="w",**csfont,fontweight="bold")
    fig.text(0.44,0.02,'Direction de jeu ---->',fontweight="bold", fontsize=15,**csfont, color="w") ###change


    plt.axhline(y=14,color="grey",alpha=0.2)
    plt.axhline(y=25,color="grey",alpha=0.2)
    plt.axhline(y=43,color="grey",alpha=0.2)
    plt.axhline(y=54,color="grey",alpha=0.2)
    plt.axhline(y=68,color="grey",alpha=0.2)

    ax.set_ylim(0,68)

    plt.tight_layout()
    
    return plt.show()


        
def pass_pct(TB,input,ptype):

    # see the custom colormaps example for more ideas on setting colormaps
    pearl_earring_cmap = LinearSegmentedColormap.from_list("Pearl Earring - 10 colors",
                                                           ['#15242e', '#4393c4'], N=10)

    # fontmanager for google font (robotto)
    robotto_regular = FontManager()

    path_eff = [path_effects.Stroke(linewidth=3, foreground='black'),
                path_effects.Normal()]



    pitch = VerticalPitch(pitch_type=ptype, line_zorder=2, pitch_color='w',pitch_width=68, pitch_length=105)
    fig, axs = pitch.grid(endnote_height=0.03, endnote_space=0,
                          title_height=0.08, title_space=0,
                          # Turn off the endnote/title axis. I usually do this after
                          # I am happy with the chart layout and text placement
                          axis=False,
                          grid_height=0.84)
    fig.set_facecolor('w')

    # heatmap and labels
    bin_statistic = pitch.bin_statistic_positional(TB.Start_x, TB.Start_y, statistic='count',
                                                   positional='full', normalize=True)
    pitch.heatmap_positional(bin_statistic, ax=axs['pitch'],
                             cmap=pearl_earring_cmap, edgecolors='#22312b')
    labels = pitch.label_heatmap(bin_statistic, color='#f4edf0', fontsize=18,
                                 ax=axs['pitch'], ha='center', va='center',
                                 str_format='{:.0%}', path_effects=path_eff)

    # endnote and title
    axs['title'].text(0.5, 0.9, input, color='black',
                      va='center', ha='center',font='tahoma', fontsize=15)
    return plt.show()
    