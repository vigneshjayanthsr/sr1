# -*- coding: utf-8 -*-
"""
Created on Fri May 28 11:04:51 2021

@author: VigneshJayanth
"""

import pandas as pd
import numpy as np
import requests
   
'''The following code returns all the competition names and ids required to refresh the player level
and P900 data'''    

def return_comp_ids():
    
    comp_ids=[]
    COMPETITIONS={}
    
    token='9d3cfe4c2601a2be325c'

    Comp_id=requests.get(f'https://skillcorner.com/api/competitions/?coverage=full&token={token}').json()['results']
    Comp_id=pd.DataFrame(Comp_id)   
    Comp_id.columns=['id','Area','Name']
    Comp_id['Comp_name']=Comp_id['Area']+'-'+Comp_id['Name']  
    
    keys = Comp_id['Comp_name']
    values = Comp_id['id']
    COMPETITIONS = dict(zip(keys, values))
    
    comp_ids.append(Comp_id.id.unique())
    comp_ids=comp_ids[0]
    
    
    return comp_ids,COMPETITIONS


g=return_comp_ids()

comps=g[1]