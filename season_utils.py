# -*- coding: utf-8 -*-
"""
Created on Tue Apr 13 17:46:43 2021

@author: VigneshJayanth
"""
#### Import the Libraries ####
import pandas as pd
import numpy as np
import requests
from comp_utils import return_comp_ids

## Calling the season class ##
class season():
    
    def __init__(self):
        
        self.token='9d3cfe4c2601a2be325c'
        comp_ids,COMPETITIONS=return_comp_ids()
        self.comps=comp_ids
        
    
    def season_schedule(self):
        
        A=[]

        
        for i in self.comps:
               
            Comps=requests.get(f'https://skillcorner.com/api/competitions/{i}/editions?&token={self.token}').json()['results']
            Comps1=pd.DataFrame(Comps).rename({'name': 'league_name','id': 'season_id'}, axis=1)
            Comps1['League_Id']=i

            
            A.append(Comps1)

        All_league_dates=pd.concat(A) 
        All_league_dates=pd.concat([All_league_dates.drop(['season'], axis=1), All_league_dates['season'].apply(pd.Series)], axis=1)
        All_league_dates = All_league_dates.rename({'name': 'Season'}, axis=1)
        All_league_dates.Season=All_league_dates.Season.str.replace('/','-')
        All_league_dates=All_league_dates[['season_id','league_name','Season','League_Id']].rename({'season_id': 'id'}, axis=1)
            
        return(All_league_dates)
        
